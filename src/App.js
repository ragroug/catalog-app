import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import HomePage from './containers/HomePage/HomePage';

import './App.css';

function App() {

  return (
      <BrowserRouter>
            <div className="App">
              <header className="App-header">

                  <HomePage />
              </header>
            </div>
      </BrowserRouter>
  );
}

export default App;
