import React from 'react';
import { connect } from 'react-redux';
import PhotoItem from "../../../Components/PhotoItem/PhotoItem";

const Wishlist = ( props ) => {
    console.log(props);
    let photosdiv = null;
    photosdiv = (
        <div>
            {props.wish.map((photo, index) => {
                return (
                        <PhotoItem
                            id={photo.id}
                            title={photo.title}
                            url={photo.url}
                            key={photo.id}
                        />
                )
            })}
        </div>
    );
    return (
        <div>
            {photosdiv}
        </div>
    )
};
const mapStateToProps = state => {
    return {
        wish: state.wishlist
    };
};

export default connect(mapStateToProps)(Wishlist);
