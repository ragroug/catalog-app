import React from 'react';
import { Route, Switch } from 'react-router-dom';
import WishList from './WishList/WishList';
import Albums from './Albums/Albums';
import PhotoList from './PhotoList/PhotoList';
import Photo from './PhotoList/Photo/Photo';




function HomePage() {

    return (

        <div>
            <Switch>
                <Route path="/" exact component={Albums} />
                <Route path="/wishlist" exact component={WishList} />
                <Route path="/photolist/:id" exact component={PhotoList} />
                <Route path="/photo/:id" exact component={Photo} />

            </Switch>
        </div>
    );
}

export default HomePage;
