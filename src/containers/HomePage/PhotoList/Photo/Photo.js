import React from 'react';
import axios from "axios";
import {connect} from "react-redux";

const Photo =   (props ) => {
    console.log(props);
    var photo =
        {
            albumId: 1,
            id: 1,
            title: "accusamus beatae ad facilis cum similique qui sunt",
            url: "https://via.placeholder.com/600/92c952",
            thumbnailUrl: "https://via.placeholder.com/150/92c952"
        };
    axios.get('https://jsonplaceholder.typicode.com/photos/' + props.match.params.id).then( (response) => {
          photo = response.data;
        console.log(photo);
    });
    const AddToWishListHandler = () => {
       console.log(props.match.params.id + 'added to wishlist')
        props.onAddWish(photo);
    }
    return (
        <div>
            <button type="button" onClick={AddToWishListHandler}>
                Add to Wishlist
            </button>
            <img src ={photo.url} alt = {photo.title}/>
            <p>{photo.title}</p>

        </div>
    )
};
const mapDispatchToProps = dispatch => {
    return {
        onAddWish: (val) => dispatch({type: 'ADDWISH', value: val })
    };
};
export default connect(null,mapDispatchToProps)(Photo);
