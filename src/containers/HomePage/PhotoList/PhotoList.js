import React from 'react';
import axios from "axios";
import {Link} from "react-router-dom";
import PhotoItem from "../../../Components/PhotoItem/PhotoItem";

const PhotoList = (props ) => {
    console.log(props);
    var photos = [
        {
            albumId: 1,
            id: 1,
            title: "accusamus beatae ad facilis cum similique qui sunt",
            url: "https://via.placeholder.com/600/92c952",
            thumbnailUrl: "https://via.placeholder.com/150/92c952"
        },
        {
            albumId: 1,
            id: 2,
            title: "reprehenderit est deserunt velit ipsam",
            url: "https://via.placeholder.com/600/771796",
            thumbnailUrl: "https://via.placeholder.com/150/771796"
        },
        {
            albumId: 1,
            id: 3,
            title: "officia porro iure quia iusto qui ipsa ut modi",
            url: "https://via.placeholder.com/600/24f355",
            thumbnailUrl: "https://via.placeholder.com/150/24f355"
        }
    ];
    axios.get('https://jsonplaceholder.typicode.com/photos?albumId=' + props.match.params.id).then((response) => {
        photos = response.data;
        console.log(photos);
    });
    let photosdiv = null;
    photosdiv = (
        <div>
            {photos.map((photo, index) => {
                return (
                    <Link to={'/photo/' + photo.id} key={photo.id}>
                        <PhotoItem
                            id={photo.id}
                            title={photo.title}
                            url={photo.url}
                        />
                    </Link>
                )
            })}
        </div>
    );
    return (
        <div>
            {photosdiv}

        </div>
    )
};

export default PhotoList;
