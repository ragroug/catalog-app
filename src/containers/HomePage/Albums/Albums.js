import React from 'react';
import { Link} from 'react-router-dom';
import {connect} from "react-redux";
import Album from '../../../Components/Album/Album';
import { fetchAlbums } from '../../../store/actions'



const Albums = (props) => {
    console.log(props);
    props.dispatch(fetchAlbums());
    let albs = null;
    albs = (
        <div>
            {this.albums.map((album, index) => {
                return (
                    <Link to={'/photolist/' + album.id} key={album.id}>
                            <Album
                            userId={album.userId}
                            id={album.id}
                            title={album.title}
                            />
                    </Link>
                )
            })}
        </div>
    );

    return (

        <div>
            <Link to="/wishlist">
                <button type="button">
                    Wishlist
                </button>
            </Link>
            {albs}
        </div>
    );
}
const mapStateToProps = state => {
    return {
        albums: state.albums
    };
};


export default connect(mapStateToProps)(Albums);
