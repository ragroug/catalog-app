const initialState = {
    wishlist: [],
    albums: []
}
const reducer = async (state = initialState, action) => {
    switch(action.type){
        case 'ADDWISH' :
            var newwish = state.wishlist;
            newwish.push(action.value);
            return {
                ...state,
                wishlist: newwish
            };
        case 'FETCH_ALBUMS' :
            return {
                ...state,
                albums : action.payload
            };
        default :
            return state;
    }
}
export default reducer;
