import axios from 'axios';

export const fetchAlbums = () => {
    return dispatch => {
            axios.get('https://jsonplaceholder.typicode.com/albums').then(res => {
                dispatch(discoverAlbum(res.data, res.status))
            }).catch(err => {
                dispatch(fetchProductsFailure(err))
            });
    };
};

export const discoverAlbum = (result, status) => {
    return {
        type: 'FETCH_ALBUMS',
        payload: result,
        status: status
    };
};
export const fetchProductsFailure = error => ({
    type: 'FETCH_PRODUCTS_FAILURE',
    payload: { error }
});


