import React from 'react';

const photoItem = (props ) => {
    return (
        <div>
            <img src ={props.url} alt = {props.title}/>
        </div>
    )
};

export default photoItem;
